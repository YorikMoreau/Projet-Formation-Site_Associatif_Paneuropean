<?php
/////////////////////////////////////////////////////SESSION///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////SCRIPTS,FRAMEWORKS,ADMINAJAX////////////////////////////////////////////////////////////////////////////////

//Hook Javavscript,Bootstrap,AdminAjax.php,Creation du "nonce",de "l'action", et de l'url de admin-ajax.php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	if(is_page_template('Nos_Balades.php')){
		$url = get_template_directory_uri()."-child/";
		wp_enqueue_script( 'balade_inscri', get_stylesheet_directory_uri() . '/js/balade_inscri.js', array(), '1.0.0', true );
		wp_localize_script( 'mon-script-ajax', 'adminAjax', admin_url('admin-ajax.php'));
		wp_localize_script('balade_inscri','baladeVar', array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'action' => 'balade_inscri',
			'site_url'=>"$url",
			'nonce' => wp_create_nonce('nos_balades_nonce')
		));
	}
	if(is_page('panregion')){
		$url = get_template_directory_uri()."-child/";
		wp_enqueue_script( 'pan_region', get_stylesheet_directory_uri() . '/js/panregion.js', array(), '1.0.0', true );
	}
	if (is_page('bonnes-adresses')) {
		$url = get_template_directory_uri() . "-child/";
		wp_enqueue_script('bonnes_adresses', get_stylesheet_directory_uri() . '/js/bonnesadresses.js', array(), '1.0.0', true);
		wp_localize_script('mon-script-ajax', 'adminAjax', admin_url('admin-ajax.php'));
		wp_localize_script('bonnes_adresses', 'bonnesadressesVar', array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'action' => 'bonnes_adresses_ajax',
			'site_url' => "$url",
			'nonce' => wp_create_nonce('bonnes_adresses_nonce')
		));
	}
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap/js/bootstrap.min.js', array( 'jquery' ), null, true );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
//Scripts Jquery UI
function jquery_jquery_ui() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		// Google API (CDN)
		wp_enqueue_script('imageMapResizer', 'https://cdnjs.cloudflare.com/ajax/libs/image-map-resizer/1.0.10/js/imageMapResizer.min.js', false, '1.0.10', true);
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js', false, '1.10.1', true);
		wp_register_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('jquery'), "1.10.3", true);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('imageMapResizer');
		
	}
}

///////////////////////////////////////////////////////////FONCTIONS LE Bureau///////////////////////////////////////////////////////////////////////////////////////
add_action('init','Le_Bureau');
function 
Le_Bureau()
{
	// PREPARATION DES REQUETES
	global $wpdb;
	$table_bureau = $wpdb->prefix . 'bureau';
	$sql_Le_Bureau = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE President = 'O' OR Secretaire = 'O' OR Tresorier = 'O'", "");
	$sql_Les_Adjoints = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Vice_Pre = 'O' OR Tres_Adj = 'O' OR Secr_Adj = 'O'", "");
	$sql_Conseil_Administration = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE MbCA='O' ", "");
	$sql_Contacts_Region = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Cont_Region = 'O' ", "");
	$sql_Webmaster = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Webmaster = 'O' ", "");
	$sql_Redaction_Pan_Info = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Redaction = 'O' ", "");
	$sql_Admin_Forum = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Admin_Forum = 'O' ", "");
	$sql_Moderateur_Forum = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE Moderateur_Forum = 'O' ", "");
	$sql_Controle_Gestion = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE CGest = 'O' ", "");
	$sql_Responsables_Region = $wpdb->prepare("SELECT * FROM  " . $table_bureau . " WHERE RR = 'O' ", "");
	// RECUPERATION DES DONNEES
	$Le_Bureau['Le_Bureau'] = $wpdb->get_results($sql_Le_Bureau);
	$Le_Bureau['Les_Adjoints'] = $wpdb->get_results($sql_Les_Adjoints);
	$Le_Bureau['Conseil_Administration'] = $wpdb->get_results($sql_Conseil_Administration);
	$Le_Bureau['Contacts_Region'] = $wpdb->get_results($sql_Contacts_Region);
	$Le_Bureau['Webmaster'] = $wpdb->get_results($sql_Webmaster);
	$Le_Bureau['Redaction_Pan_Info'] = $wpdb->get_results($sql_Redaction_Pan_Info);
	$Le_Bureau['Admin_Forum'] = $wpdb->get_results($sql_Admin_Forum);
	$Le_Bureau['Moderateur_Forum'] = $wpdb->get_results($sql_Moderateur_Forum);
	$Le_Bureau['Controle_Gestion'] = $wpdb->get_results($sql_Controle_Gestion);
	$Le_Bureau['Responsables_Region'] = $wpdb->get_results($sql_Responsables_Region);
	
	return $Le_Bureau;
}

add_action('init','Role_Membre');
function 
Role_Membre($Membre)
{
	if ($Membre->President == "O") {
		$Role_Membre = "President";
		return $Role_Membre;
	}
	else if ($Membre->Secretaire == "O") {
		$Role_Membre = "Secretaire";
		return $Role_Membre;
	}
	else if ($Membre->Tresorier == "O") {
		$Role_Membre = "Tresorier";
		return $Role_Membre;
	}
	else if ($Membre->Vice_Pre == "O") {
		$Role_Membre = "Vice-President";
		return $Role_Membre;
	}
	else if ($Membre->Tres_Adj == "O") {
		$Role_Membre = "Tresorier Adjoint";
		return $Role_Membre;
	}
	else if ($Membre->Secr_Adj == "O") {
		$Role_Membre = "Secretaire Adjoint";
		return $Role_Membre;
	}
	else if ($Membre->Cont_Region == "O") {
		$Role_Membre = "Coordinateur régional";
		return $Role_Membre;
	}
	else if ($Membre->Redaction == "O") {
		$Role_Membre = "Redacteur Pan Info";
		return $Role_Membre;
	}
	else if ($Membre->Admin_Forum == "O") {
		$Role_Membre = "Administrateur Forum";
		return $Role_Membre;
	}
	else if ($Membre->Moderateur_Forum == "O") {
		$Role_Membre = "Moderateur Forum";
		return $Role_Membre;
	}
	else if ($Membre->CGest == "O") {
		$Role_Membre = "Controle Gestion";
		return $Role_Membre;
	}
	else if ($Membre->Webmaster == "O") {
		$Role_Membre = "Webmaster";
		return $Role_Membre;
	}
	else {
		$Role_Membre = "?"; 
		return $Role_Membre;
	}
}




///////////////////////////////////////////////////////////FONCTIONS NOS BALADES///////////////////////////////////////////////////////////////////////////////////

//hook les scripts Ajax et Admin AjaxPhp
add_action('init', 'jquery_jquery_ui');
if (isset($_POST['id'])) 
{
	add_action('wp_ajax_balade_inscri','balade_inscri');
	add_action('wp_ajax_nopriv_balade_inscri','balade_inscri');   
}
//Fonction qui reçoit l'id des balades depuis le fichier admin-ajax.php
//l'envoie dans une requete sql qui ramène seulement les données balades de l'id concerné
// et renvoie les données au fichier JavaScript
function balade_inscri($id)	
{
	check_ajax_referer('nos_balades_nonce', 'nonce');
	if(isset($_POST['id']))
	{
		$id = $_POST['id'];

		$balades = affiche_annees_balades($id);
		// $balades += $url;
		$output = json_encode($balades);

		if(is_array($output)){
		
			print_r($output);
		}
		else{
			echo $output;
		}
		die();
		//return $output;
	}
}
//Ramene les Balades
function nos_balades(){
	global $wpdb;
	$table_balades= $wpdb->prefix.'nos_balades';
	$sql = $wpdb->prepare("SELECT Photo, Region, DateD, DateF, Balade, Organisateur, Descript, Num, Num2 FROM ".$table_balades." ORDER BY DateD DESC LIMIT 20","");
	$listeBalades = $wpdb->get_results($sql);
	return $listeBalades;
}
//Renvoie les années de toutes les balades
function annees_balades(){
	global $wpdb;
	$table_balades= $wpdb->prefix.'nos_balades';
	$sql = $wpdb->prepare("SELECT YEAR(DateD) AS annees FROM ".$table_balades." GROUP BY YEAR(DateD) DESC","");
	$anneesBalades = $wpdb->get_results($sql);
	return $anneesBalades;
	
}
//Convertion des dates au format français
function dateToFrench($date, $format) 
{
	$english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
	$english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
	return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
}
// function mon_image(){
// 	return'<img src="http://localhost:8080/wordpress/wp-content/uploads/2019/10/espresso-1024x614.jpg" alt="mon image" />';
// }
// // modifiez le hook de la fonction the_excerpt(), avec un hook filtre, afin de remplacer celle-ci par la nouvelle fonction
// add_filter('twenty_seventeen_excerpt_more', 'excerpt_new');
// add_action( 'wp_enqueue_scripts', 'add_css_and_js' );



///////////////////////////////////////////////////////////FONCTIONS Bonnes Adresses///////////////////////////////////////////////////////////////////////////////////

// 	//hook les scripts Ajax et Admin AjaxPhp
if (isset($_POST['region'])) {
	add_action('wp_ajax_bonnes_adresses_ajax', 'bonnes_adresses_ajax');
	add_action('wp_ajax_nopriv_bonnes_adresses_ajax', 'bonnes_adresses_ajax');
}
// 	//Fonction qui reçoit l'id des balades depuis le fichier admin-ajax.php
// 	//l'envoie dans une requete sql qui ramène seulement les données balades de l'id concerné
// 	// et renvoie les données au fichier JavaScript
function bonnes_adresses_ajax($region)
{
	check_ajax_referer('bonnes_adresses_nonce', 'nonce');
	if (isset($_POST['region'])) {
		$region = $_POST['region'];

		$bonnes_adresses = affiche_region_bonnes_adresses($region);
		// $balades += $url;
		$output = json_encode($bonnes_adresses);

		if (is_array($output)) {

			print_r($output);
		} else {
			echo $output;
		}
		die();
		//return $output;
	}
}
// //Renvoie les balades par années
function affiche_region_bonnes_adresses($region)
{
	global $wpdb;
	$prepare = $region;
	$table_balades = $wpdb->prefix . "bonnes_adresses";
	$sql = $wpdb->prepare("SELECT * FROM ".$table_balades. " WHERE Region='".$prepare."' ORDER BY Region ASC",$prepare);
	$listeBalades = $wpdb->get_results($sql);
	return $listeBalades;
}
add_action('init', 'affiche_region_bonnes_adresses');
// 	//Renvoie les balades par années
function affiche_annees_balades($id){
	global $wpdb;
	$prepare = $id;
	$table_balades= $wpdb->prefix."nos_balades";
	$sql = $wpdb->prepare("SELECT Photo, Region, DateD, DateF, Balade, Organisateur, Descript, Num, Num2 FROM " . $table_balades . " WHERE YEAR(DateD) = '%d' ORDER BY DateD DESC", $prepare);
	$listeAdresses = $wpdb->get_results($sql);
	return $listeAdresses;
}
add_action('init', 'affiche_annees_balades');
function bonnes_adresses()
{
	global $wpdb;
	$table_balades = $wpdb->prefix . 'bonnes_adresses';
	$sql = $wpdb->prepare("SELECT * FROM " . $table_balades . " ORDER BY Region ASC", "");
	$BonnesAdresses = $wpdb->get_results($sql);
	return $BonnesAdresses;
}
add_action('init','regions_bonnes_adresses');
function 
regions_bonnes_adresses()
{
	global $wpdb;
	$table_balades = $wpdb->prefix . 'bonnes_adresses';
	$sql = $wpdb->prepare("SELECT Region FROM " . $table_balades . " GROUP BY Region ASC", "");
	$regions_bonnes_adresses = $wpdb->get_results($sql);
	return $regions_bonnes_adresses;
}

///////////////////////////////////////////////////////////FONCTIONS ADMIN///////////////////////////////////////////////////////////////////////////////////
add_action('template_redirect', 'insert_balade');

function insert_balade()
{
	global $wpdb;
	if (isset($_POST['insert_balade_submit']) && isset($_POST['insert_balade-verif'])) {
		if (wp_verify_nonce($_POST['insert_balade-verif'], 'insert_balade')) {
			$input1 = ($_POST['input1']);
			$input2 = ($_POST['input2']);
			$id_session = ($_POST['id_session']);
			$_POST['test'] = $id_session;
			if ($input1 == $input2) {
				$url = wp_get_referer();
				$table_membre = $wpdb->prefix . 'membre';
				$update_pass_sql = $wpdb->prepare("UPDATE " . $table_membre . " SET passe = '%s' WHERE pseudo = '%d'", "$input1", "$id_session");
				$update_pass = $wpdb->query($update_pass_sql);
				$_POST['statut'] = 'success';
				wp_safe_redirect($url);
			}
		}
	}
}
add_action('template_redirect','updatePass');

function updatePass(){
	global $wpdb;
	if(isset($_POST['update_pass_submit']) && isset($_POST['update_pass-verif'])){
		if (wp_verify_nonce($_POST['update_pass-verif'], 'update_pass')) {
			$input1 = ($_POST['input1']);
			$input2 = ($_POST['input2']);
			$id_session = ($_POST['id_session']);
			$_POST['test'] = $id_session;
			if($input1 == $input2){
				$url = wp_get_referer();
				$table_membre = $wpdb->prefix . 'membre';
				$update_pass_sql = $wpdb->prepare("UPDATE ".$table_membre." SET passe = '%s' WHERE pseudo = '%d'","$input1","$id_session" );
				$update_pass = $wpdb->query($update_pass_sql);	
				$_POST['statut'] = 'success';
				wp_safe_redirect($url);
			}
		}
		
	}
}

add_action('template_redirect','deconnexionMembre');

function deconnexionMembre(){
	$url = site_url('',  null);
	if(isset($_POST['deconnexion_submit']) && isset($_POST['deconnexion-verif'])){
		if (wp_verify_nonce($_POST['deconnexion-verif'], 'deconnexion')) {
			setcookie('id_session','',time(),"/wordpress");
			unset($_COOKIE['id_session']);
			wp_safe_redirect($url);			
		}
	}
}

add_action('template_redirect','connexionMembre');

function connexionMembre(){
	$url = site_url('',  null);
	if(isset($_POST['connexion_submit']) && isset($_POST['connexion-verif'])){
		if (wp_verify_nonce($_POST['connexion-verif'], 'connexion')) {
			$pass = ($_POST['password']);
			$membre = ($_POST['membre']);
						
			if ($membre == "") {
				$url = wp_get_referer();
				$_POST['statut'] = 'court_m';
				setcookie('id_session','',time(),"/wordpress");
				unset($_COOKIE['id_session']);
				wp_safe_redirect($url);
			}
			else if ($pass == "") {
				$url = wp_get_referer();
				$_POST['statut'] = 'court_p';
				setcookie('id_session','',time(),"/wordpress");
				unset($_COOKIE['id_session']);
				
				wp_safe_redirect($url);
			}
			else{
				global $wpdb;
				$table_membre = $wpdb->prefix . 'membre';
				$sql_membre = $wpdb->prepare("SELECT pseudo FROM " . $table_membre . " WHERE pseudo = '%d'", $membre);
				$verif_membre = $wpdb->get_results($sql_membre);
				$sql_pseudo = $wpdb->prepare("SELECT passe FROM " . $table_membre . " WHERE pseudo ='%d'", $membre);
				$verif_pass = $wpdb->get_results($sql_pseudo);
				foreach ($verif_pass[0] as $key => $value) {
					$verif_pass = $value;
				}
				if(!$verif_membre){
					$url = wp_get_referer();
					// $url = add_query_arg('status', 'membre_not_exist', wp_get_referer());
					$_POST['statut'] = 'membre_not_exist';
					setcookie('id_session','',time(),"/wordpress");
					unset($_COOKIE['id_session']);
					wp_safe_redirect($url);
				}
				else if(!$verif_pass){
					$url = wp_get_referer();
					$_POST['statut'] = 'pass_not_exist';
					setcookie('id_session','',time(),"/wordpress");
					unset($_COOKIE['id_session']);
					wp_safe_redirect($url);
				}
				else{
					if ($pass == $verif_pass) {
						$site = site_url('',  null);
						$id = uniqid();
						setcookie('id_session',$id,time()+3600,"/wordpress");
						$_COOKIE['id_session'] = $id;
						$table_session = $wpdb->prefix . 'session';
						$exist_session = $wpdb->prepare("SELECT id_session FROM ".$table_session." WHERE id_session = '%s'","$id" );
						$exist_session = $wpdb->get_results($exist_session);
						foreach ($exist_session as $key => $value) {
							$exist_session = $value;
						}
						if($exist_session == $id){
							$sql_session = $wpdb->prepare("UPDATE".$table_session." SET session_id = '%s', n_adherant = '%s', logon = NOW() WHERE id_session = '%s'","$id", $membre, $id);
							$wpdb->query($sql_session);
						}
						else{
							$sql_session = $wpdb->prepare("INSERT INTO ".$table_session." (session_id,n_adherent,logon) Value ('%s','%s',NOW())", $id, $membre);
							$wpdb->query($sql_session);						
						}
						
						$url = wp_get_referer();
						wp_safe_redirect($url);
					}
					else {
						$url = wp_get_referer();
						$_POST['statut'] = 'pass_not_exist';
						setcookie('id_session','',time(),"/wordpress");
						unset($_COOKIE['id_session']);
						wp_safe_redirect($url);
					}
				}
				
			}
		}
	}
}
add_action('init', 'Verif_Privilege');
function Verif_Privilege($id_session){
	global $wpdb;
	$table_bureau = $wpdb->prefix . 'bureau';
	$table_session = $wpdb->prefix . 'session';
	$sql_verif_privilege = $wpdb->prepare("SELECT ".$table_bureau.".Niv from ".$table_bureau." left join ".$table_session." ON ".$table_bureau.".Num = ".$table_session.".n_adherent WHERE ".$table_session.".session_id = '%s' ",$id_session);
	$privilege = $wpdb->get_results($sql_verif_privilege);
	return $privilege;
}
add_action('init', 'mes_infos');
function mes_infos($id_session){
	global $wpdb;
	$table_membre = $wpdb->prefix . 'membre';
	$table_session = $wpdb->prefix . 'session';
	$sql_mes_infos = $wpdb->prepare("SELECT * from ".$table_membre." join ".$table_session." ON ".$table_membre.".pseudo = ".$table_session.".n_adherent WHERE ".$table_session.".session_id = '%s' ",$id_session);
	$mes_infos = $wpdb->get_results($sql_mes_infos);
	return $mes_infos;
}
add_action('init', 'mon_pseudo');
function mon_pseudo($id_session){
	global $wpdb;
	$table_membre = $wpdb->prefix . 'membre';
	$table_session = $wpdb->prefix . 'session';
	$sql_mon_pseudo = $wpdb->prepare("SELECT pseudo from ".$table_membre." join ".$table_session." ON ".$table_membre.".pseudo = ".$table_session.".n_adherent WHERE ".$table_session.".session_id = '%s' ",$id_session);
	$mon_pseudo = $wpdb->get_results($sql_mon_pseudo);
	foreach ($mon_pseudo as  $value) {
		$mon_pseudo = $value;
	}
	return $mon_pseudo;
}

add_action('init', 'liste_orgas');
function liste_orgas(){
	global $wpdb;
	$table_adherents = $wpdb->prefix . 'adherents';
	$sql_liste_orgas = $wpdb->prepare("SELECT Nom, Prenom from ".$table_adherents." ORDER BY Nom DESC","");
	$liste_orgas = $wpdb->get_results($sql_liste_orgas);
	// foreach ($liste_orgas as  $value) {
	// 	$liste_orgas[] += $value;
	// }
	return $liste_orgas;
}


?>




