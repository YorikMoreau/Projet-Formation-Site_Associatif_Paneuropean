<?php
/*
   Template Name: Ma_fiche
*/
// Version 2019/11/14
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif; ?>
        <!-- ////////////////////////insérer du contenu/////////////////////////////////////////////////////////  -->
            <div class="container">
                <div class="row">
                    <h1>Ma fiche: L'ensemble de mes informations détenues par l'Amicale Pan-European</h1>
                </div>            
                <div class="row">
                    <table class="table table-dark">
                        <tbody>
                            <?php if (isset($_COOKIE['id_session'])) { 
                                $id_session = $_COOKIE['id_session'];?> 
                                   <?php $mes_infos = mes_infos($id_session);
                                    foreach ($mes_infos[0] as $key => $value) {
                                        echo '<tr>';
                                        if($key == "passe"){
                                            echo '<td><p>'.$key.'</p><a class="fiche-link" href="'.site_url('/upload-passe', null).'">Modifier mot de passe</a></td><td><p>'.$value.'</p></td>';
                                        }
                                        else{
                                            echo '<td><p>'.$key.'</p></td><td><p>'.$value.'</p></td>';
                                        }
                                        echo '</tr>';
                                    } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        
            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>