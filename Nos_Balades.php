<?php
/*
   Template Name: Nos Balades
*/
// Version 2016-09-29
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif;
        ////////////////////////insérer du contenu///////////////////////////////////////////////////////// 
        ?>

        <?php $listeBalades = nos_balades();
              $listeAnnees  = annees_balades(); ?>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <select class=form-control>
                    <option value="choix selected">Choisissez une année</option>
                    <?php
                    foreach ($listeAnnees as $value) {
                        if ($value->annees != 0) {
                            ?>
                            <option value="<?= $value->annees ?>"><?= $value->annees ?></option>;
                    <?php }
                    } ?>
                </select>
            </nav>
            <div class="My_balades">
                <?php
                // print_r($listeAnnees);
                if ($listeBalades) {
                    $date = "";
                    $limite_annonces = 20;
                    foreach ($listeBalades as $key => $value) {
                        // $limite_annonces--;
                        if ($limite_annonces != 0) {
                            if ($date != dateToFrench($value->DateD, "F Y")) {
                                $date = dateToFrench($value->DateD, "F Y"); ?>
                                <h1><?= dateToFrench($value->DateD, "F Y"); ?></h1>
                                <hr>
                            <?php
                                        }
                                        ?>
                            <div class="row line">
                                <div class="col-3">
                                    <?php if (empty($value->Photo)) { ?>
                                        <img class="LogoRegion" src="<?= site_url('wp-content/themes/twentyseventeen-child/IMAGES/FRANCAIS.GIF',  null) ?>" alt="">
                                    <?php } else { ?>
                                        <img class="LogoRegion" src="<?= $value->Photo ?>" alt="">
                                    <?php } ?>
                                    <?php if ($value->Region == "France") { ?>
                                        <p> </p>
                                    <?php } else { ?>
                                        <p>Balade organisée par la région <?= $value->Region ?></p>
                                    <?php } ?>
                                </div>
                                <div class="col-6">
                                    <p>du <?= dateToFrench($value->DateD, "j/m/Y") ?> au <?= dateToFrench($value->DateF, "j/m/Y") ?> - <?= $value->Balade ?></p>
                                    <p> <?= $value->Organisateur ?></p>
                                    <p> <?= $value->Descript ?></p>
                                    <?php if (strtotime(date('Y-m-d')) < strtotime($value->DateD)) { ?>
                                        <button type='button' id="clickMe <?= $key ?>" class="clickMe <?= $key ?> btn btn-success">Faites Vos Réservations!</button>
                                        <a href="">Fiche Balade</a>
                                    <?php } else { ?>
                                        <a href="">Lire le(s) commmentaire(s)</a>
                                    <?php } ?>
                                </div>
                                <div class="col-3">
                                    <img class="Trombine" src="<?= site_url('wp-content/themes/twentyseventeen-child/IMAGES/Trombines/Small/' . $value->Num . '.jpg',  null) ?>" alt="">
                                    <?php if (!empty($value->Num2)) {
                                                    ?>
                                        <img class="Trombine2" src="<?= site_url('wp-content/themes/twentyseventeen-child/IMAGES/Trombines/Small/' . $value->Num2 . '.jpg',  null) ?>" alt="">
                                    <?php
                                                } else { } ?>
                                </div>
                            </div>
                            <div class="row underline">
                                <div class="slideMe <?= $key ?>">
                                    Blablabla... Vachement inspiré comme gars
                                </div>
                            </div>
                            <hr>
                    <?php   } else {
                                break;
                            }
                        }
                    } else { ?>
                    <div class="btop no-suscribers">Personne n’est enregistré</div>
                <?php   } ?>
            </div>

        </div>

            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>