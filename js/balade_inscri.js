jQuery(document).ready(function (event) {
	function extraitNombre(str) { 
		return Number(str.replace(/[^\d]/g, "")) 
	}
	$('.slideMe').hide();
	$('.clickMe').on('click', function () {
		$id = $(this).attr('class');
		$id = extraitNombre($id);
		if($(this).hasClass($id)){
			$(".slideMe").each(function () {
				if($(this).hasClass($id)){
					// alert(this);
					if($(this).hasClass('ajax')){
						$(this).slideToggle(500);
						e.preventDefault();
					}
					else{
						$(this).slideToggle(500);
					}
				}
			});
		}
		return false;
	});
	$('.form-control').change(function () {
		var id = $(this).val();
		id = parseInt(id);
		$('.My_balades').empty();
		affiche_ajax_balades(id);
		// $("script[src='" + $site + "js/balade_inscri.js?ver=1.0.0'").remove();
	});
	function affiche_ajax_balades(id) {
		
		jQuery.ajax({
			type: "POST",
			dataType: 'JSON',
			url: baladeVar.ajaxurl,
			data: {
				action: baladeVar.action,
				nonce: baladeVar.nonce,
				id: id
			},
			success: function (data,localisation) {
				$site = baladeVar.site_url;
				console.log($site);
				console.log(baladeVar);
				$('.My_balades').empty();
				$.getScript($site+"js/balade_inscri.js", function () {});
				$.each(data, function (key, value) {
					////////////////INITIALISATION DES VARIABLES//////////////////////////////////////////////////////////////////////
					$DateD = this['DateD'];
					$DateF = this['DateF'];
					$DateD_brute = this['DateD'];
					$DateD_split = $DateD_brute.split("-");
					$DateD_my_french_year = $DateD_split[0];
					$DateD_my_english_month = $DateD_split[1];
					$DateD_CENTER_PART = $DateD.replace('-', '/');
					$DateF_CENTER_PART = $DateF.replace('-', '/');
					$english_months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
					$french_months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
					$DateD_CENTER_PART = convertDigitIn($DateD_CENTER_PART);
					$DateF_CENTER_PART = convertDigitIn($DateF_CENTER_PART);
					$photo = this['Photo'];
					$Region = this['Region'];
					$Balade = this['Balade'];
					$Num = this['Num'];
					$Num2 = this['Num2'];
					$Organisateur = this['Organisateur'];
					$Descript = this['Descript'];
					$.each($english_months, function (index, value) {
						if (value == $DateD_my_english_month) {
							$DateD_my_french_month = $french_months[index];
						}
					});
					function convertDigitIn(str) {
						return str.replace('-', '/').split('/').reverse().join('/');
					}
					///////////////////////TESTS SUR LES VARIABLES//////////////////////////////////////////////////////////////////////
					if($photo == ""){
						$logo = "<img class='LogoRegion' src='"+ $site +"IMAGES/FRANCAIS.GIF' alt='' >";
					}
					else{
						$logo = "<img class='LogoRegion' src='"+ $photo +"' alt=''>";
					}
					if ($Region == "France") {
						$logo_subtitle = "";
						
					}
					else{
						$logo_subtitle = "<p>Balade organisée par la région " + $Region + " </p>";
					}
					if($Num2 == ""){
						$Num2 = "";
					}
					else{
						$Num2 = "<img class='Trombine' src=' " + $site + "IMAGES/Trombines/Small/" + $Num2 + ".jpg' alt='photo organisateur'>";
					}
					/////////////////////////////////Construction de la page en retour d'ajax///////////////////////////////////////////
					$line = "<h1>" + $DateD_my_french_month + " " + $DateD_my_french_year + "</h1>" +
						"<hr><div class='row line'>" +
						"<div class='col-3'>" +
						$logo +
						$logo_subtitle +
						"</div>" +
						"<div class='col-6'>" +
						"<p>du " + $DateD_CENTER_PART + " au " + $DateF_CENTER_PART + " - " + $Balade + " </p>" +
						"<p>" + $Organisateur + "</p>" +
						"<p>" + $Descript + "</p>" +
						"<button type='button' id='clickMe" + key + "' class='clickMe " + key + " btn btn-success'>Faites Vos Réservations!</button>" +
						"<a href=''>Fiche Balade</a>" +
						"</div>" +
						"<div class='col-3'>" +
						"<img class='Trombine' src=' " + $site +"IMAGES/Trombines/Small/"+ $Num +".jpg' alt='photo organisateur'>" +
						$Num2 +
						"</div>" +
						"</div>" +
						"<div class='row underline'>" +
						"<div class='slideMe " + key + " ajax'>" +
						"<p>" + $site + "</p>" +
						"</div>" +
						"</div>";
						$('.My_balades').append($line);
					});
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				$('.line').append('error');
			}
		});
	}
});

