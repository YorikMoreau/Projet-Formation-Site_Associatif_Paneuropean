jQuery(document).ready(function (event) {
    
    $('.form-control').change(function () {
        var region = $(this).val();
        affiche_ajax_adresses(region);
        // $("script[src='" + $site + "js/balade_inscri.js?ver=1.0.0'").remove();
    });
    function affiche_ajax_adresses(region) {

        jQuery.ajax({
            type: "POST",
            dataType: 'JSON',
            url: bonnesadressesVar.ajaxurl,
            data: {
                action: bonnesadressesVar.action,
                nonce: bonnesadressesVar.nonce,
                region: region
            },
            success: function (data, localisation) {
                $site = bonnesadressesVar.site_url;
                console.log($site);
                // console.log(bonnesadressesVar);
                $('.my_bonnes_adresses').empty();
                $.getScript($site + "js/bonnesadresses.js", function () { });
                $line =
                "   <table class='table'>" +
                "       <thead class='thead-light table-bordered'>" +
                "           <tr>" +
                "               <th scope='col'>Recommandé par:</th>"+
                "               <th scope='col'>Type</th>"+
                "               <th scope='col'>Etablissement</th>"+
                "               <th scope='col'>Adresse</th>"+
                "               <th scope='col'>Region</th>"+
                "           </tr>"+
                "       </thead>"+
                "       <tbody>";
                $.each(data, function (key, value) {
                    $type = this['Type'];
                    $enseigne = this['Enseigne'];
                    $adresse = this['Adresse'];
                    $region = this['Region'];
                    $line += 
                    "<tr>"+
                    "    <td></td>"+
                    "    <td>"+$type+"</td>"+
                    "    <td>"+$enseigne+"</td>"+
                    "    <td>"+$adresse+"</td>"+
                    "    <td>"+$region+"</td>"+
                    "</tr>";
                });
                $line += 
                "       </tbody>"+
                "   </table>";    
                $('.my_bonnes_adresses').append($line);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('.line').append('error');
            }
        });
    }
});