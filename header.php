<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei&display=swap" rel="stylesheet">

	<?php
	wp_head();
	?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'twentyseventeen'); ?></a>
		<header id="masthead" class="site-header" role="banner">

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<a class="navbar-brand" href="#">L'Amicale Pan European</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="<?= site_url('',  null); ?>">Portail <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://www.amicale-paneuropean.com/forum/index.php">Forum</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= site_url('/espace-public',  null); ?>">Espace Public</a>
						</li>
						<?php if (isset($_COOKIE['id_session'])) { ?>
							<li class="nav-item">
								<a class="nav-link" href="<?= site_url('/Admin',  null); ?>">Espace Admin</a>
							</li><?php } ?>
						<li>
							<?php if(!isset($_COOKIE['id_session'])){
								
								?><form action='#' method='POST' class='form form-inline my-2 my-lg-0'>
									<?php wp_nonce_field('connexion', 'connexion-verif'); ?>
									<p>
										<input class="form-control mr-sm-2" type="text" name="membre" placeholder="n°adhérent" aria-label="Search">
										<input class="form-control mr-sm-2" type="password" name="password" placeholder="mot de passe" aria-label="Search">
										<!-- <input type="number" id="don" name="don" value="0"/> -->
									</p>
									<p>
										<button id="submit" type="submit" name="connexion_submit" class="submit">Envoyer</button>
									</p>
								</form><?php
							
							} else {
							
								?><form action='#' method='POST' class='form form-inline my-2 my-lg-0'>
									<?php wp_nonce_field('deconnexion', 'deconnexion-verif'); ?>
									<p>
										<button id="submit" type="submit" name="deconnexion_submit" class="submit">Deconnexion</button>
									</p>
								</form><?php
							
							}?>
						</li>
					</ul>
				</div>
			</nav>
			<?php if (!is_front_page()){ ?>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="<?= site_url('/le-bureau',  null); ?>">Le Bureau</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= site_url('/nos-balades',  null); ?>">Balades</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Actualités</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= site_url('/panregion',  null); ?>">Pan région</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Pan-Info</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Inscription</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Annonces</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= site_url('/bonnes-adresses',  null); ?>">Bonnes Adresses</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Photos/Vidéos</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Techniques</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Liens</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Flyer!</a>
						</li>
					</ul>
				</div>
			</nav>
			<?php } ?>
	</div>

	</header><!-- #masthead -->

	<?php
	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ((is_single() || (is_page() && !twentyseventeen_is_frontpage())) && has_post_thumbnail(get_queried_object_id())) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail(get_queried_object_id(), 'twentyseventeen-featured-image');
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>
	<div class="site-content-contain">
		<?php if (function_exists('fil_ariane')) {
			echo fil_ariane();
		} ?>
		<div id="content" class="site-content">
			<?php
				if (isset($_COOKIE['id_session'])) {
					echo   '<div class="alert alert-success alert-dismissible fade show" role="alert">
							  <div>Vous êtes connectés</div>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
							</div>';
				} 
			 else if (isset($_POST['statut'])) {
				echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
				switch ($_POST['statut']) {
					case 'court_m':
						echo   '<div>veuillez saisir votre numéro d\'adhérent</div>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>';

						break;
					case 'court_p':
						echo   '<div>veuillez saisir un mot de passe</div>
								    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>';
						break;
					case 'membre_not_exist':
						echo   '<div>Adhérent non trouvé</div>
									  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									    <span aria-hidden="true">&times;</span>
									  </button>';
						break;
					case 'pass_not_exist':
						echo   '<div>mot de passe incorrect!</div>
									  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									    <span aria-hidden="true">&times;</span>
									  </button>';

						break;

					default:
						echo   '<div>Une erreur est survenue<div>
									  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									    <span aria-hidden="true">&times;</span>
									  </button>';
				}
				echo '</div>';
			}

			?>