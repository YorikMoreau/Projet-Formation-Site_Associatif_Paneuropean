<?php
/*
   Template Name: Admin
*/
// Version 2019/11/14
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif; ?>
        <!-- ////////////////////////insérer du contenu/////////////////////////////////////////////////////////  -->
        <div class="container">
 
        <?php if (isset($_COOKIE['id_session'])) { 
            $id_session = $_COOKIE['id_session'];
            $Niv_privilege = Verif_Privilege($id_session);
            foreach ($Niv_privilege[0] as $key => $value) {
                $Niv_privilege = $value;
            }
            echo  $id_session;
            if($Niv_privilege >= 0){
                if (isset($_POST['test'])) {
                    echo $_POST['test'];
                }
                echo '<h1>Pour tous</h1>';

                echo '<h4><a class="nav-link" href="'.site_url('/ma-fiche', null).'">Ma fiche: L\'ensemble de mes informations détenues par l\'Amicale Pan-European</a></h4>';
               
                echo '<h4>Le bureau: Adresser un mail au bureau de l\'Amicale Pan-European</h4>';
            }
            if($Niv_privilege >= 2){
                $liste_orgas = liste_orgas();
                // foreach ($liste_orgas as $key => $value) {
                //  echo   "<p>".  $value->Nom."</p>";
                // }
                // print_r($liste_orgas);
                echo '<h1>Vu par les membres du conseil d\'administration et les responsables régionaux.</h1>';
                echo '<h4>Saisie Actu: 	Saisie de l\'actualité de votre region</h4>';
                echo '<h4><a class="nav-link" href="'.site_url('/saisie-balade', null).'">Saisie Balade: Saisie des sorties de votre region</a></h4>';
                echo '<h4>Liste adhérents: Recevoir la liste des adhérents de la région par Mail</h4>';
                echo '<h4>Liste non renouvelés: Liste des adhérents de la région n\'ayant pas renouvelé leur cotisation</h4>';
                echo '<h4>Les inscriptions: Liste des inscriptions en attente de validation..</h4>';
                echo '<h4>Album photos: Administrez les photos de l\'album.</h4>';
            }
            if($Niv_privilege >= 4){
                echo '<h1>Vu par les membres du conseil d\'administration.</h1>';
                echo '<h4>Fiches adhérents: Modification des informations des adhérents.</h4>';
                echo '<h4>Balades nationales: Saisie des balades nationales</h4>';
                echo '<h4>Liste adhérents: 	Recevoir la liste des adhérents par mail</h4>';
                echo '<h4>Liste non renouvelés: Recevoir la liste des adhérents n\'ayant pas renouvelé par mail</h4>';
                echo '<h4>Relance cotisations: 	Envoyer un mail de relance aux retardataires.</h4>';
                echo '<h4>Relance inscrits: Envoyer un mail de relance aux inscriptions non validées.</h4>';
            }
            if($Niv_privilege >= 6){
                echo '<h1>Vu par le président et le trésorier uniquement.</h1>';
                echo '<h4>Inscriptions: Gestion des inscriptions.</h4>';
                echo '<h4>Supprimer un adhérent: ATTENTION - Suppression d\'un adhérent.</h4>';
                echo '<h4>Réintégrer un adhérents: Ré-intégration d\'un adhérent présent dans les archives.</h4>';
            }
            if($Niv_privilege == 8){
                echo '<h1>Zone de debug Webmaster.</h1>';
                echo '<h4>zone Web  Master</h4>';
            }
        } ?>         
        </div>   
            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>