<?php
/*
   Template Name: panregion
*/
// Version 2016-09-29
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif;
        ////////////////////////insérer du contenu///////////////////////////////////////////////////////// 
        ?>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <img class="displayed img-fluid" src="https://zupimages.net/up/19/46/vj7o.png" usemap="#map"  border="0">
                        <map class="map" name="map">
                        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
                        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
                        <!-- #$-:Please do not edit lines starting with "#$" -->
                        <!-- #$VERSION:2.3 -->
                        <!-- #$AUTHOR:Yorik -->
                        <area class="bretagne" shape="poly" coords="12,176,13,171,19,167,14,164,26,161,45,159,55,165,58,160,68,162,69,152,72,151,88,150,97,164,105,177,117,166,130,170,139,165,142,170,156,172,163,183,173,177,176,177,176,187,174,193,175,210,173,210,166,224,161,218,154,220,152,224,148,227,133,229,127,232,124,237,122,241,114,243,112,238,99,239,104,233,92,228,86,228,82,231,74,224,65,218,53,216,49,213,43,204,40,210,32,207,31,214,29,214,28,208,19,199,34,196,34,187,19,183,38,184,37,179,32,176,26,174" alt="bretagne" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwil-f7o--TlAhXK7eAKHdA7AH0QFjAAegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FBretagne&amp;usg=AOvVaw2NF6GXFImZqoEYrqmwjWwM">
                        <area shape="poly" coords="145,94,150,102,148,107,158,125,159,139,160,155,158,157,167,167,161,173,163,178,170,170,178,176,187,172,193,178,217,172,222,173,226,184,234,178,245,179,248,189,258,193,258,188,263,183,261,174,256,167,256,161,243,156,242,146,242,136,238,122,219,130,183,121,171,122,172,117,166,107,168,98,156,103" alt="basse normandie" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiGjdWq9uTlAhWM2BQKHdx6CwQQFjAAegQIAxAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FBasse-Normandie&amp;usg=AOvVaw1fHTCQ07IaOzcALZGc8t6v">
                        <area shape="poly" coords="235,110,240,94,275,87,289,77,301,88,304,94,302,99,302,105,303,114,300,134,297,140,289,142,290,150,284,159,280,160,264,165,264,157,252,151,247,152,250,139,246,127,243,112" target="haute normandie" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjxwsa99uTlAhXM7eAKHcE7Bu4QFjAAegQIAxAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FHaute-Normandie&amp;usg=AOvVaw21SxFRjL0L-hZ7a0PcOGSM">
                        <area shape="poly" coords="293,73,296,68,307,72,306,64,299,59,309,57,321,66,331,67,333,79,339,72,351,79,360,81,366,79,381,79,398,78,404,82,411,82,412,89,411,95,401,104,404,108,404,114,399,115,385,125,387,133,385,142,387,147,382,153,373,144,370,135,362,138,357,139,349,138,341,134,329,131,320,130,311,131,309,121,310,110,307,107,311,93,296,73" alt="picardie" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=2&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjr_PKP9-TlAhVh5eAKHS51D68QFjABegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FPicardie&amp;usg=AOvVaw1Y3Tk_f7vOkl7qpG8Rcdf-">
                        <area shape="poly" coords="338,60,324,62,314,52,305,50,302,51,298,22,310,14,338,8,341,14,338,22,352,38,361,28,367,32,368,48,377,49,385,47,387,63,394,62,399,58,409,59,406,67,407,75,403,73,389,71,374,74,360,72,351,69,340,67" alt="nord pas de calais" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwistYSj9-TlAhXV8uAKHdW-BvsQFjAAegQIAhAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FNord-Pas-de-Calais&amp;usg=AOvVaw1egUTIujHRZSuuNpKEymf6">
                        <area shape="poly" coords="295,145,304,138,321,137,343,145,364,143,379,157,378,164,380,174,376,186,361,186,358,192,360,196,355,201,351,199,344,203,341,195,336,191,324,187,318,190,316,180,310,178,308,172,304,169,302,162,302,152" alt="Île-de-France" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=19&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiIjbC49-TlAhWeA2MBHZK6B4kQFjASegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2F%25C3%258Ele-de-France&amp;usg=AOvVaw17bZKdItl38phIDQvVkDXh">
                        <area shape="poly" coords="418,84,417,100,410,108,410,126,403,123,392,126,392,130,399,136,392,138,392,141,396,145,384,162,388,174,383,185,391,193,391,200,397,201,405,215,418,213,429,213,430,208,444,209,456,224,453,228,462,232,469,235,471,228,479,229,486,217,485,212,479,209,478,198,467,189,466,183,448,174,447,163,441,159,446,145,443,139,444,124,449,115,449,104,457,100,446,96,438,94,436,74,432,83" alt="Champagne-Ardenne" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=2&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjkuurX9-TlAhXtDmMBHZ-TAM0QFjABegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FChampagne-Ardenne&amp;usg=AOvVaw2JHUAh9TDuOVWiRF-LrPAe">
                        <area shape="poly" coords="456,107,459,117,454,125,452,134,455,144,451,155,455,168,470,177,477,184,489,197,487,205,493,210,507,205,508,211,529,209,534,215,548,188,540,183,541,166,550,162,550,156,538,159,531,146,537,134,561,136,535,130,527,134,515,116,500,111,491,115,480,109,468,113" alt="lorraine" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiLu83v9-TlAhW1DWMBHbEQCCcQFjAAegQICRAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FLorraine&amp;usg=AOvVaw0nBpCmxf_EcCUTjD4DAFCH">
                        <area shape="poly" coords="544,141,565,146,571,140,569,136,588,138,585,149,582,149,579,156,574,160,571,177,565,198,567,209,563,229,565,234,565,243,559,244,555,240,550,235,548,223,544,219,544,211,550,201,549,196,556,187,550,182,549,175,558,166,557,152,545,147" alt="alsace" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjxucKZ-OTlAhUKEBQKHVoUAnQQFjAAegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FAlsace&amp;usg=AOvVaw33P7UbKzyWpAe3MUHf6jIM">
                        <area shape="poly" coords="479,238,489,234,491,226,501,214,520,219,526,218,542,228,543,238,547,239,536,252,542,254,533,269,526,275,516,284,515,291,503,305,501,312,501,321,494,326,489,321,482,320,481,326,477,318,480,309,477,308,480,301,477,295,485,286,475,284,472,279,480,267,480,254,479,246" alt="franche compte" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiBzdqz-OTlAhWD5-AKHdkwATIQFjAAegQIAhAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FFranche-Comt%25C3%25A9&amp;usg=AOvVaw1wfxMcBEdnFNH0f-3KT0Ik">
                        <area shape="poly" coords="362,201,369,198,366,192,382,192,385,195,385,204,390,208,396,206,400,222,420,221,435,218,437,215,443,218,448,223,446,230,451,237,461,238,466,248,473,241,465,251,474,260,467,278,463,291,468,292,472,305,470,313,460,309,449,311,444,327,440,323,425,326,421,333,411,333,414,322,411,307,404,311,390,292,383,299,368,298,371,282,368,271,362,256,364,239,362,235,368,223,371,212" alt="bourgogne" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwizidrJ-OTlAhXT7eAKHSq-CgwQFjAAegQIAxAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FBourgogne&amp;usg=AOvVaw3G-LH2IPi3htjiiGtbSBIK">
                        <area shape="poly" coords="266,169,290,164,291,159,297,156,296,166,301,177,311,189,314,197,329,198,334,198,332,206,339,210,351,208,360,207,363,215,359,222,359,229,347,229,355,241,351,245,353,255,351,261,359,272,361,290,353,298,347,297,337,305,338,310,323,316,302,315,299,318,290,316,286,320,281,318,273,306,254,271,245,282,239,273,233,263,238,244,256,241,258,234,268,225,269,194,275,183,273,183,274,182" alt="centre" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiTu_Hg-OTlAhV8A2MBHfgPDwwQFjAAegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FCentre&amp;usg=AOvVaw0PUmZVyYO-26QU7D3Eb9aG">
                        <area shape="poly" coords="183,210,180,181,185,180,195,186,218,180,224,190,233,190,237,183,242,195,251,202,257,201,261,205,260,215,255,225,247,231,229,239,227,252,220,269,192,272,192,277,179,276,178,285,184,294,190,321,177,319,163,322,147,315,134,298,128,283,136,270,130,266,133,256,124,252,117,257,110,254,113,247,134,241,136,235,156,229,171,229,179,218" alt="pays de la loire " href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjbl_f5-OTlAhWrAWMBHbmTDAsQFjAAegQICBAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FPays_de_la_Loire&amp;usg=AOvVaw2G3rk54zs_VdNO7QGfUOsF">
                        <area shape="poly" coords="188,283,194,291,199,312,202,325,191,333,173,328,168,334,176,344,168,367,186,381,188,389,200,389,204,400,212,403,217,401,220,395,228,393,227,381,239,375,241,365,255,357,256,330,271,319,260,310,262,301,251,286,235,289,234,280,224,274,218,283,214,275,200,282" alt="charente poitou " href="https://fr.wikipedia.org/wiki/Poitou-Charentes">
                        <area shape="poly" coords="266,332,270,353,258,366,260,372,274,373,279,381,289,391,283,397,285,403,290,405,290,412,299,411,308,420,319,411,325,396,328,390,330,380,334,383,337,371,328,360,336,354,340,345,334,331,328,330,324,323,306,324,304,329,294,325,288,331,283,327,274,328" alt="limousin " href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=2&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiArMqi-eTlAhUpBGMBHQ-5DR8QFjABegQIBRAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FLimousin&amp;usg=AOvVaw1Ogaw9-XxnYl9VCjpfxUgn">
                        <area shape="poly" coords="332,321,337,317,350,317,349,308,350,305,357,305,362,301,375,310,383,308,389,308,392,305,396,315,406,321,403,325,396,329,396,347,390,353,392,359,392,369,404,381,402,393,405,398,415,400,419,394,425,397,430,403,423,410,425,412,417,420,405,429,394,420,389,424,381,414,366,421,362,430,350,414,341,421,336,436,329,437,328,427,325,423,326,414,330,404,336,396,345,393,346,368,340,363,353,349,343,327" alt="auvergne " href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwiWt4u_-eTlAhVSAGMBHV9gDrgQFjAAegQIARAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FAuvergne&amp;usg=AOvVaw0SNJ48T2NQDUj_x2N2r2L_">
                        <area shape="poly" coords="404,338,408,354,401,355,400,364,413,380,412,387,425,387,434,391,439,403,435,417,421,428,411,434,414,446,421,458,440,456,443,457,450,458,460,448,466,454,466,459,472,459,473,464,484,467,488,463,478,459,480,442,485,443,488,430,494,431,498,421,504,425,507,418,519,417,511,408,515,396,527,398,528,403,552,393,552,382,547,372,538,362,538,351,546,347,536,329,536,322,525,324,521,332,512,340,497,340,499,331,474,332,465,319,458,317,448,341,440,341,439,333,433,332,426,342" alt="rhone alpes " href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjoxb_b-eTlAhWgA2MBHerbDLMQFjAAegQIBBAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FRh%25C3%25B4ne&amp;usg=AOvVaw3_IWlJWiBbQboEQe-9MPuV">
                        <area shape="poly" coords="166,385,151,499,136,523,149,526,150,535,157,543,182,548,185,558,195,558,199,542,212,529,208,521,208,515,198,512,202,494,200,483,208,481,211,484,218,481,231,477,234,479,240,475,250,477,257,467,258,451,264,452,262,441,269,439,275,432,283,423,282,411,274,409,274,396,278,390,275,387,271,388,270,383,266,379,258,382,251,376,242,383,238,390,233,402,223,406,215,416,202,408,194,407,194,399,187,416" alt="aquitaine" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjE4ab5-eTlAhVYA2MBHWPYB-EQFjAAegQIAhAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FAquitaine&amp;usg=AOvVaw1vC4qZi9XqN5B_P3-XIU8n">
                        <area shape="poly" coords="211,508,214,493,226,492,226,486,255,485,268,477,267,464,281,457,273,451,287,434,295,422,295,417,306,427,315,423,320,429,319,442,322,451,332,445,343,444,349,428,353,430,359,444,367,459,371,474,375,479,376,486,373,490,361,490,362,498,354,503,344,504,340,519,322,516,322,520,303,519,292,539,298,546,308,548,311,558,303,559,303,569,310,574,307,578,299,572,287,570,283,564,274,567,268,560,255,559,247,553,239,557,241,568,228,565,219,563,214,567,205,559,206,553,216,546,222,528,217,509" alt="Midi-Pyrénées" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwj2jNqW-uTlAhUOkhQKHQUlC00QFjAAegQIABAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FMidi-Pyr%25C3%25A9n%25C3%25A9es&amp;usg=AOvVaw1yl3d6Of2wJIgLtstaDc0u">
                        <area shape="poly" coords="308,586,331,576,324,564,313,567,320,559,320,540,307,538,310,527,334,532,330,525,358,526,351,515,376,506,376,502,394,483,386,476,386,468,378,466,374,449,368,439,371,427,373,433,378,422,385,438,394,432,401,434,407,455,415,470,427,470,438,467,442,477,447,484,441,491,441,498,431,500,429,510,411,509,369,536,356,537,354,549,361,554,360,583,346,591,337,588" alt="languedoc roussillon " href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwivyve0-uTlAhUF2-AKHZR7Dr4QFjAAegQICBAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FLanguedoc-Roussillon&amp;usg=AOvVaw0BIyU1kWTXSp1IEfpvCRmz">
                        <area shape="poly" coords="528,413,536,411,542,421,553,426,549,437,544,447,548,465,570,472,588,469,589,475,581,482,580,492,565,496,561,505,546,506,549,516,532,525,535,530,519,538,505,537,484,532,483,522,478,522,470,524,459,514,450,518,440,516,448,506,451,495,461,487,456,472,465,468,491,482,503,466,500,454,491,453,499,439" alt="Provence-Alpes-Côte d'Azur" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjIyITM-uTlAhWGDxQKHbZgAIYQFjAAegQIAxAB&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FC%25C3%25B4te_d%2527Azur&amp;usg=AOvVaw2VfBd0z0PE8Ih2HcGbclTG">
                        <area shape="poly" coords="639,330,635,226,618,219,613,224,617,227,616,236,600,233,587,240,579,239,569,244,551,254,539,275,530,286,515,305,513,316,543,313,559,344,592,334,617,298" alt="suisse" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwj7jZXf-uTlAhXK8eAKHYOhCAgQFjAAegQIBxAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FSuisse&amp;usg=AOvVaw34g_SAbCnYUakrMlbS7F5-">
                        <area shape="poly" coords="341,5,490,25,500,32,498,37,506,45,505,52,490,59,476,87,482,97,476,103,471,103,464,93,444,87,447,79,445,77,446,62,437,61,427,72,426,76,418,77,418,69,417,66,421,56,405,46,393,51,389,39,374,39,375,27,368,18,355,21,353,26,347,20,347,13" alt="belgique" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwjPq7vy-uTlAhWRDxQKHVdQBRgQFjAAegQIBxAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FBelgique&amp;usg=AOvVaw12LdhPFO2Okc7oTd1Mpo85">
                        <area shape="poly" coords="677,539,684,553,681,564,687,573,689,653,645,653,637,582,664,554" alt="corse" href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=2ahUKEwispO6A--TlAhXy8OAKHeWFCl8QFjAAegQIBhAC&amp;url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FCorse&amp;usg=AOvVaw0Yp8C-xXwkLZK3PFlEc9JE">
                        </map>
                    </div>
                </div>
            </div>

            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>