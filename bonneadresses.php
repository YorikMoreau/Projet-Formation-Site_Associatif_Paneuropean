<?php
/*
   Template Name: Bonnes adresses
*/
// Version 2016-09-29
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif;
        ////////////////////////insérer du contenu///////////////////////////////////////////////////////// 
        ?>
        <?php $BonnesAdresses = bonnes_adresses();
        $RegionsBonnesAdresses =  regions_bonnes_adresses(); ?>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <select class=form-control>
                    <option value="choix selected">Choisissez une Region</option>
                    <?php
                    foreach ($RegionsBonnesAdresses as $value) { ?>
                        <option value="<?= $value->Region ?>"><?= $value->Region ?></option>;
                    <?php
                    } ?>
                </select>
            </nav>
            <div class="row my_bonnes_adresses">
                <table class="table">
                    <thead class="thead-light table-bordered">
                        <tr>
                            <th scope="col">Recommandé par:</th>
                            <th scope="col">Type</th>
                            <th scope="col">Etablissement</th>
                            <th scope="col">Adresse</th>
                            <th scope="col">Region</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if ($BonnesAdresses) {
                            foreach ($BonnesAdresses as $key => $value) { ?>

                                <!-- <div class="col-2">

                        </div>
                        <div class="col-1">
                            
                        </div>
                        <div class="col-5">

                        </div>
                        <div class="col-3">

                        </div>
                        <div class="col-1">

                        </div> -->
                                <tr>
                                    <td></td>
                                    <td><?= $value->Type ?></td>
                                    <td><?= $value->Enseigne ?></td>
                                    <td><?= $value->Adresse ?></td>
                                    <td><?= $value->Region ?></td>
                                </tr>
                        <?php }
                        }

                        ?>

                    </tbody>
                </table>
            </div>
        </div>

            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>