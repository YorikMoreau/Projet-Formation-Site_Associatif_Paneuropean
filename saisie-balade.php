<?php
/*
   Template Name: Saisie_Balade
*/
// Version 2019/11/14
get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php if (function_exists('mon_image')) {
    echo mon_image();
} ?>

<?php if (function_exists('posts_actu')) {
    posts_actu(5);
} ?>
<?php
////////////////////////insérer du contenu/////////////////////////////////////////////////////////
if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- Ce qui suit teste si l'Article en cours est dans la Catégorie 3. -->
        <!-- Si c'est le cas, le bloc div reçoit la classe CSS "post-cat-three". -->
        <!-- Sinon, le bloc div reçoit la classe CSS "post". -->
        <?php if (in_category('3')) { ?>
            <div class="post-cat-three">
            <?php } else { ?>
                <div class="post">
                <?php } ?>

                <div class="topHead" align="center">
                    <!-- Affiche le Titre en tant que lien vers le Permalien de l'Article. -->
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <!-- Affiche la Date. -->
                    <small><?php the_time('F jS, Y'); ?></small>
                </div>

                <!-- Affiche le corps (Content) de l'Article dans un bloc div. -->
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                </div> <!-- Fin du premier bloc div -->

                <!-- Fin de La Boucle (mais notez le "else:" - voir la suite). -->
            <?php endwhile;
            else : ?>

            <!-- Le premier "if" testait l'existence d'Articles à afficher. Cette -->
            <!-- partie "else" indique que faire si ce n'est pas le cas. -->
            <p>Sorry, no posts matched your criteria.</p>

            <!-- Fin REELLE de La Boucle. -->
        <?php endif; ?>
        <!-- ////////////////////////insérer du contenu/////////////////////////////////////////////////////////  -->
            
            <div class="container">
                <div class="row">
                    <h1>Saisie Balade: Saisie des sorties de votre region</h1>
               </div>            
                <div class="row">
                    <div class="col">
                        <form action='#' method='POST' class='form '>
                        <?php wp_nonce_field('insert_balade', 'insert_balade-verif') ?>
                            <div class='form-group col-12'>
                                <input class='form-control col-12' type='text' name='balade' placeholder='titre de la balade' >
                            </div>
                            <div class='form-group col-12'>
                                <input class='form-control col-12' type='date' name='DateD' placeholder='date de début'>
                            </div>
                            <div class='form-group col-12'>
                                <input class='form-control  col-12' type='date' name='DateF' placeholder='date de fin'>
                            </div>
                            <div class='form-group col-12'>
                                <div class='form-check'>
                                    <input class='form-check-input' type='checkbox' name='TDF' >
                                    <label class='form-check-label'>Cette balade est une étape du tour de france ?</label>
                                </div>
                            </div>
                            <div class='form-group col-12'>
                               <label for='Descript'>Description de la balade</label>
                               <textarea class='form-control col-12' name='descript' rows='3'></textarea>
                            </div>
                            <div class='form-group col-12'>
                            <select class='form-control col-12' name='Organisateur'>
                                <option selected>Organisateur</option>
                                <?php $liste_orgas = liste_orgas(); 
                                foreach ($liste_orgas as $key => $value) { 
                                echo '<option>'.$value->Nom.' '.$value->Prenom.'</option>';
                             } ?>
                            </select>
                            </div>
                            <div class='form-group col-12'>
                            <select class='form-control col-12' name='Co-Organisateur'>
                                <option selected>Co-Organisateur</option>
                                <?php $liste_orgas = liste_orgas(); 
                                    foreach ($liste_orgas as $key => $value) { 
                                    echo '<option>'.$value->Nom.' '.$value->Prenom.'</option>';
                                } ?>
                            </select>
                            </div>
                            <div class='form-group col-10'>
                                <input class='form-control  col-12' type='text' name='link' placeholder='Lien vers un site '>
                            </div>
                            <div class='form-group col-10'>
                                <input class='form-control  col-12' type='text' name='Picture' placeholder='Lien vers une image '>
                            </div>
                            <div class='form-group col-12'>
                            <label for='Descript'>Prévenir l'attaché de presse ?</label>
                            <select class='form-control col-12' name='Co-Organisateur'>
                                <option selected>Oui</option>
                                <option>Non</option>
                            </select>
                            </div>
                            <button id='submit' type='submit' name='insert_balade_submit' class='btn btn-primary'>Envoyer</button>
                        </form>
                    </div>
                </div>
            </div>
        
            </div><!-- .entry-content -->

            <?php
            get_footer(); // On affiche de pied de page du thème
            ?>