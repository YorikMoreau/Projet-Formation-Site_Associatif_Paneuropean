<?php
   /*
   Template Name: produits
   */
   // Version 2016-09-29

   get_header(); // On affiche l'en-tête du thème WordPress
?>

<?php
   /////////////////////////////////////////Le Bureau/////////////////////////////////////////////////////////////////
   // global $wpdb; // On se connecte à la base de données du site
   // $lebureau = $wpdb->get_results("
   //    SELECT Num FROM `bureau` WHERE Nom = 'Rolle' AND Prenom = 'Guy'  ;
   // ");
   // foreach ($lebureau as $key => $value) {
   //    $lebureau = $value->Num;
   // }
   
?>
   <h1 class="entry-title">Le Bureau</h1>		<span class="edit-link"><a class="post-edit-link" href="http://localhost/wordpress/wp-admin/post.php?post=96&#038;action=edit">Modifier<span class="screen-reader-text"> "Le Bureau"</span></a></span>	</header><!-- .entry-header -->
   <figure class="wp-block-image"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Coure-1.jpg" alt="" class="wp-image-98"/></figure>
		
   <p>Guy ROLLÉ / Pascal JANODET / Alain VERHULLE / Daniel LE BADEZET / Marie-Hélène CHALMÉ /Jackie POULIN / Claude BROUILLON / Isabelle DROUET</p>
   <?php 
      echo $lebureau;
   ?>

   <h2>Le Bureau</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/espresso-1024x614.jpg" alt="" class="wp-image-5" width="113" height="68" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/espresso-1024x614.jpg 1024w, http://localhost/wordpress/wp-content/uploads/2019/10/espresso-300x180.jpg 300w, http://localhost/wordpress/wp-content/uploads/2019/10/espresso-768x461.jpg 768w" sizes="(max-width: 113px) 100vw, 113px" /></figure>

   <h2>Les Adjoints</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/sandwich-1024x614.jpg" alt="" class="wp-image-6" width="112" height="67" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/sandwich-1024x614.jpg 1024w, http://localhost/wordpress/wp-content/uploads/2019/10/sandwich-300x180.jpg 300w, http://localhost/wordpress/wp-content/uploads/2019/10/sandwich-768x461.jpg 768w" sizes="(max-width: 112px) 100vw, 112px" /></figure>

   <h2>Le Conseil d&rsquo;Administration</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="83" height="83" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 83px) 100vw, 83px" /></figure>

   <h2>Contacts Régions</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="72" height="72" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 72px) 100vw, 72px" /></figure>

   <h2>WebMaster</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="66" height="66" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 66px) 100vw, 66px" /></figure>

   <h2>Rédaction PAN-Info</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="92" height="92" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 92px) 100vw, 92px" /></figure>

   <h2>Administrateur Forum</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="88" height="88" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 88px) 100vw, 88px" /></figure>

   <h2>Moderateur Forum</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="76" height="76" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 76px) 100vw, 76px" /></figure>

   <h2>Le Controle de Gestion</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="70" height="70" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 70px) 100vw, 70px" /></figure>

   <h2>Les Responsables Regionaux (D.R.A.P.E)</h2>

   <figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif" alt="" class="wp-image-99" width="76" height="76" srcset="http://localhost/wordpress/wp-content/uploads/2019/10/Logo.gif 150w, http://localhost/wordpress/wp-content/uploads/2019/10/Logo-100x100.gif 100w" sizes="(max-width: 76px) 100vw, 76px" /></figure>

</div><!-- .entry-content -->

<?php 
   get_footer(); // On affiche de pied de page du thème
?>