<?php
   /*
   Template Name: Le Bureau
   */
   // Version 2016-09-29

   get_header(); // On affiche l'en-tête du thème WordPress
?>
   
   <div class="container-fluid">
       <div class="row" align="center">
         <figure ><img id="Myheader" src="<?= site_url( 'wp-content/themes/twentyseventeen-child/IMAGES/Bureau/bureau_2018_2019_WEB_2.jpg', null ); ?>" alt="" /></figure>
         <p>Guy ROLLÉ / Pascal JANODET / Alain VERHULLE / Daniel LE BADEZET / Marie-Hélène CHALMÉ /Jackie POULIN / Claude BROUILLON / Isabelle DROUET</p>
      </div>
      <?php $Chemin_Trombines = site_url( 'wp-content/themes/twentyseventeen-child/IMAGES/Trombines/Small/', null ); ?>
      <?php 
      $Le_Bureau = Le_Bureau();
      foreach ($Le_Bureau as $Role => $Membres_Bureau_index) {
         if ($Role == "Le_Bureau") {
            $html =  "<h1>Le Bureau</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-4 col-xs-4'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Les_Adjoints") {
            $html.= "<h1>Les Adjoints</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-4 col-xs-4'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Conseil_Administration") {
            $html.= "<h1>Le Conseil d’Administration</h1>".
            "<div class='row' align='center'>";
            $Membres_par_ligne = 0;
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Membres_par_ligne ++;
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-4'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>Membre du conseil d'administration</h6>".
                  "</div>";
               if ($Membres_par_ligne == 3) {
                  $Membres_par_ligne = 0;
                  $html.= "</div>".
                       "<div class='row' align='center'>";
               }
            }
            $html.= "</div>";
         }
         if ($Role == "Contacts_Region") {
            $html.= "<h1>Contacts Region</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               if ($Membre->Contacts_Region = 'O') {
                  $Role_Membre = "Coordinateur Region";
               }
               $html.= 
                  "<div class='col col-lg-6'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Webmaster") {
            $html.= "<h1>Webmaster</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-12'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Redaction_Pan_Info") {
            $html.= "<h1>Redacteur Pan Info</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-12'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Admin_Forum") {
            $html.= "<h1>Administrateurs Forum</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               if ($Membre->Admin_Forum = 'O') {
                  $Role_Membre = "Administrateur Forum";
               }
               $html.= 
                  "<div class='col col-lg-6'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Moderateur_Forum") {
            $html.= "<h1>Moderateurs Forum</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               if ($Membre->Moderateur_Forum = 'O') {
                  $Role_Membre = "Moderateur Forum";
               }
               $html.= 
                  "<div class='col col-lg-6'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Controle_Gestion") {
            $html.= "<h1>Controle Gestion</h1>".
            "<div class='row' align='center'>";
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-6'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>".$Role_Membre."</h6>".
                  "</div>";
            }
            $html.= "</div>";
         }
         if ($Role == "Responsables_Region") {
            $html.= "<h1>Les Responsables Regionaux (D.R.A.P.E)</h1>".
            "<div class='row' align='center'>";
            $Membres_par_ligne = 0;
            foreach ($Membres_Bureau_index as $key => $Membre) {
               $Membres_par_ligne ++;
               $Role_Membre = Role_Membre($Membre);
               $html.= 
                  "<div class='col col-lg-4'>".
                        "<div class='cadre'>".
                           "<figure class='wp-block-image'><img src='".$Chemin_Trombines.$Membre->Num.".jpg' alt='".$Role."'></figure>".
                        "</div>".
                        "<h2>".$Membre->Nom."<br>".$Membre->Prenom."</h2>".
                        "<h6>Responsable Region : <b>  ".$Membre->Region." </b></h6>".
                  "</div>";
               if ($Membres_par_ligne == 3) {
                  $Membres_par_ligne = 0;
                  $html.= "</div>".
                           "<div class='row' align='center'>";
               }
            }
            $html.= "</div>";
         }
      }
      
      echo $html;
      
      // $html.= '<pre>';
      // print_r($Le_Bureau);
      // $html.= '</pre>';
      
      
      ?>     
      

</div><!-- .entry-content -->

<?php 
   get_footer(); // On affiche de pied de page du thème
?>
